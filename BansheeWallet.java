import java.io.Serializable;
import java.util.List;

public class BansheeWallet implements Serializable {

	private static final long serialVersionUID = 1L;
	
	byte[] privatekey;
	List<Cointact> cointacts;
	List<Log> logs;
	String security;
	
	public byte[] getPrivateKey() {
		return privatekey;
	}
	
	public void setPrivateKey(byte[] privatekey) {
		this.privatekey = privatekey;
	}
	
	public List<Cointact> getCointacts() {
		return cointacts;
	}
	
	public void setCointacts(List<Cointact> cointacts) {
		this.cointacts = cointacts;
	}
	
	public List<Log> getLogs() {
		return logs;
	}
	
	public void setLogs(List<Log> logs) {
		this.logs = logs;
	}
	
	public String getSecurity() {
		return security;
	}
	
	public void setSecurity(String security) {
		this.security = security;
	}
	
	public BansheeWallet (byte[] privatekey, List<Cointact> cointacts, List<Log> logs, String security) {
		this.privatekey = privatekey;
		this.cointacts = cointacts;
		this.logs = logs;
		this.security = security;
	}
	
}
