import javax.swing.ImageIcon;

public class Cointact {
	private String address;
	private String description;
	private ImageIcon icon;
	
	public Cointact (String address, String description, ImageIcon icon) {
		this.address = address;
		this.description = description;
		this.icon = icon;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public ImageIcon getIcon() {
		return this.icon;
	}

}
