import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicButtonUI;

import qrcode.QrCode;
import verge.Wallet;

public class Ui extends JFrame {

	private static final long serialVersionUID = 1L;
    final static Color lightblue = new Color(165,217,244);
    final static Color skyblue = new Color(193,232,255);
    final static Color altblue = new Color(203,236,255);
    final static Color gold = new Color(231,220,139);
    final static Color darkblue = new Color(10,29,52);
    final static Color darkdarkblue = new Color(5,17,31);
    final static Color greyedout = new Color(52,73,94);
    final static Font buttonFont = new Font("papyrus", Font.BOLD, 18);
    final static Font homepanelFont = new Font("papyrus", Font.BOLD, 14);
    final static Font currencyFont = new Font("papyrus", Font.BOLD, 12);
    final static Font balanceFont = new Font("papyrus", Font.BOLD, 18);
    final static Font instructFont = new Font("papyrus", Font.BOLD, 24);
    final static Font monoFont = new Font("inconsolata", Font.BOLD, 12);
    final static String walletFile = System.getProperty("user.home") + System.getProperty("file.separator") + "BANSHEE.dat";
    static boolean walletExists = false;
    static boolean loggedIn = false;
    private static String passphrase = "";
    private static File entropyFile = null;
    private static ImageIcon entropyIcon = null;
    private static byte[] entropy = null;
    private static JPanel preWalletPanel = new JPanel(new CardLayout());
    private static JPanel walletPanel = new JPanel(new CardLayout());
    private static JPanel topPanel = new JPanel(new CardLayout());
    private static JLabel myaddress = new JLabel();
    private static JLabel mybalance = new JLabel();
    private static javax.swing.Timer timer;
    private static long startTime = -1;
    private static long duration = 31000;
    private static BigDecimal walletBalance = new BigDecimal("0.0");
    private static String walletAddress = "DDd1pVWr8PPAw1z7DRwoUW6maWh5SsnCcp";
    private static byte[] publickey = new byte[65];
	//private static byte[] privatekey = new byte[32];
	private static String walletimportformatkey = "";
	private static String compressedwalletimportformatkey = "";
	private static byte[] compressedpublickey = new byte[33];
	private static String address = "";
	private static String privatekeyrestore = "";
    private static String backupbansheedatfile = "";
    private static String backupentropyfile = "";
    private static String restorepassphrase = "";
    
    public static JTextArea privatekeybackuptext;
    public static JLabel privatekeybackupqrcode;
    public static JLabel mywalletaddress;
    public static BansheeWallet wallet;
    
    public Ui() {

        setUndecorated(true);

        JPanel mainPanel = mainPanel();
        JPanel setupPanel = setupPanel();
        JPanel loginPanel = loginPanel();
        
        topPanel.setOpaque(false);
        topPanel.add(mainPanel,"main");
        topPanel.add(setupPanel,"setup");
        topPanel.add(loginPanel,"login");

        JLabel wallpaperLabel = new JLabel();
        wallpaperLabel.setLayout(new FlowLayout());
        wallpaperLabel.setIcon(new ImageIcon(Ui.class.getResource("banshee_c.png")));
        wallpaperLabel.setLayout(new BorderLayout());
        wallpaperLabel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, lightblue));
        wallpaperLabel.add(topPanel);

        add(wallpaperLabel);
        
        doesWalletExist();
        if (walletExists) {
        	loadWallet();
        	((CardLayout)topPanel.getLayout()).show(topPanel, "login");
        } else {
        	loggedIn = true;
        	((CardLayout)topPanel.getLayout()).show(topPanel, "setup");
        }

    }

    private static JPanel mainPanel() {

    	walletPanel.setOpaque(false);
        JPanel homePanel = homePanel();
    	JPanel directoryPanel = directoryPanel();
        JPanel transactionsPanel = transactionsPanel();
        JPanel sendPanel = sendPanel();
        JPanel backupPanel = backupPanel();
        JPanel restorePanel = restorePanel();
        
        walletPanel.add(homePanel,"home");
        walletPanel.add(directoryPanel,"directory");
        walletPanel.add(transactionsPanel,"transactions");
        walletPanel.add(sendPanel,"send");
        walletPanel.add(backupPanel,"backup");
        walletPanel.add(restorePanel,"restore");
        
        ((CardLayout)walletPanel.getLayout()).show(walletPanel, "home");

        JPanel postWalletMenuPanel = postWalletMenuPanel();

    	JPanel mainpanel = new JPanel();
        mainpanel.setOpaque(false);
        mainpanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.NONE;
        c.weightx = 0.2;
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.WEST;
        mainpanel.add(postWalletMenuPanel,c);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridx = 1;
        c.gridy = 0;
        mainpanel.add(walletPanel,c);
        return mainpanel;
    }
    
    private static JPanel setupPanel() {

    	preWalletPanel.setOpaque(false);
    	JPanel homePanel = homePanel();
    	JPanel restorePanel = restorePanel();
    	JPanel createWalletPanel1 = createWalletPanel1();
    	JPanel createWalletPanel2 = createWalletPanel2();
    	JPanel createWalletPanel3 = createWalletPanel3();
    	JPanel createWalletPanel4 = createWalletPanel4();
        //createWalletPanel5 = createWalletPanel5();
        
        preWalletPanel.add(homePanel,"home");
        preWalletPanel.add(restorePanel,"restore");
        preWalletPanel.add(createWalletPanel1,"createwallet1");
        preWalletPanel.add(createWalletPanel2,"createwallet2");
        preWalletPanel.add(createWalletPanel3,"createwallet3");
        preWalletPanel.add(createWalletPanel4,"createwallet4");

        ((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet1");
        
        JPanel preWalletMenuPanel = preWalletMenuPanel();
        
    	JPanel setuppanel = new JPanel();
    	setuppanel.setOpaque(false);
    	setuppanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.NONE;
        c.weightx = 0.2;
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.WEST;
        setuppanel.add(preWalletMenuPanel,c);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridx = 1;
        c.gridy = 0;
        setuppanel.add(preWalletPanel,c);
        return setuppanel;
    }
    
    private static void registerFonts() {
    	try {
    			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    			Font papyrus = Font.createFont(Font.TRUETYPE_FONT, Ui.class.getResourceAsStream("PAPYRUS.TTF"));
    			ge.registerFont(papyrus);
    			Font inconsolata = Font.createFont(Font.TRUETYPE_FONT, Ui.class.getResourceAsStream("Inconsolata-Regular.ttf"));
    			ge.registerFont(inconsolata);
    		} catch (IOException|FontFormatException e) {
    		 //
    		}
    }
    
    private static BigDecimal getBalance(String address) {
    	return Transaction.getAddressBalance(address);
    }
    
    private static void setBalance() {
    	//mybalance.setText(walletBalance.toString());
    }
    
    private static void doesWalletExist() {
    	if(Files.exists(Paths.get(walletFile))) walletExists = true;
    }
    
    private static JPanel walletAddressPanel() {
    	JLabel myaddresslabel = new JLabel();
    	myaddresslabel.setText("WALLET ADDRESS:  ");
    	myaddresslabel.setForeground(lightblue);
    	myaddresslabel.setFont(homepanelFont);
    	mywalletaddress = new JLabel();
    	mywalletaddress.setForeground(gold);
    	mywalletaddress.setFont(homepanelFont);
    	JPanel walletaddresspanel = new JPanel();
    	walletaddresspanel.setOpaque(false);
    	walletaddresspanel.setLayout(new GridBagLayout());
    	walletaddresspanel.setBorder(new EmptyBorder(0,0,0,0));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.1;
        c.gridwidth = 1;
        c.insets = new Insets(0,0,0,0);
        c.anchor = GridBagConstraints.SOUTHWEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        walletaddresspanel.add(myaddresslabel, c);
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.9;
        c.gridwidth = 1;
        c.insets = new Insets(0,0,0,0);
        c.anchor = GridBagConstraints.SOUTHWEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        walletaddresspanel.add(mywalletaddress, c);
    	return walletaddresspanel;
    }
    
    private static JPanel walletBalancePanel(BigDecimal mywalletbalance) {
    	JLabel mybalance = new JLabel();
    	mybalance.setText(mywalletbalance.toString());
    	mybalance.setForeground(gold);
    	mybalance.setFont(balanceFont);
    	JLabel mybalancecurrency = new JLabel();
    	mybalancecurrency.setText("  XVG");
    	mybalancecurrency.setForeground(gold);
    	mybalancecurrency.setFont(currencyFont);
    	JPanel balancesPanel = new JPanel();
    	balancesPanel.setOpaque(false);
    	balancesPanel.setLayout(new FlowLayout());
    	balancesPanel.add(mybalance);
    	balancesPanel.add(mybalancecurrency);
    	return balancesPanel;
    }
    
    private static JPanel createWalletPanel1() {
        // First use and create a new wallet panel
        final Color titleColor = lightblue;
        final Font titleFont = new Font("papyrus", Font.BOLD, 48);
        final Font subtitleFont = new Font("papyrus", Font.PLAIN, 16);
        final Font introFont = new Font("papyrus", Font.BOLD, 24);
        JLabel titleLabel = new JLabel("BANSHEE");
        JLabel subtitleLabel = new JLabel("  V e r g e    C u r r e n c y    W a l l e t      1 . 0");
        JLabel introLabel = new JLabel("<html>Welcome to Banshee - the first, best, and only cross-platform wallet for Verge currency.<br>This wallet has never been initialized. You may restore a previous wallet or create a new one.</html>");
        JButton createAddressButton = new JButton("Create A New Wallet");
    	
        titleLabel.setForeground(titleColor);
        titleLabel.setFont(titleFont);
        subtitleLabel.setForeground(titleColor);
        subtitleLabel.setFont(subtitleFont);
        introLabel.setForeground(gold);
        introLabel.setFont(introFont);

    	createAddressButton.setUI(new BasicButtonUI());
    	createAddressButton.setFont(buttonFont);
    	createAddressButton.setBackground(gold);
    	createAddressButton.setForeground(darkblue);
    	createAddressButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet2");
    		}
    	});

    	JPanel newPanel = new JPanel();
        newPanel.setOpaque(false);
        newPanel.setLayout(new GridBagLayout());
        newPanel.setBorder(new EmptyBorder(10,40,10,40));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 0.10;
        c.anchor = GridBagConstraints.LAST_LINE_END;
        newPanel.add(titleLabel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1.0;
        c.weighty = 0.10;
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        newPanel.add(subtitleLabel, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        newPanel.add(introLabel, c);
        c.gridx = 0;
        c.gridy = 3;
        c.fill = GridBagConstraints.NONE;
        c.weightx = 1.0;
        c.weighty = 0.10;
        c.anchor = GridBagConstraints.PAGE_END;
        newPanel.add(createAddressButton, c);
        return newPanel;
   }
    
    private static JPanel createWalletPanel2() {
    	final Font introFont = new Font("papyrus", Font.BOLD, 24);
        JButton nextButton = new JButton("Next");
        JLabel introLabel = new JLabel();
        JTextField passphraseinput1 = new JTextField(255);
        JLabel helperLabel = new JLabel();
        JButton backButton = new JButton("Back");
        
        introLabel.setText("<html>Step 1.  Enter a Passphrase:<br></html>");
        introLabel.setForeground(gold);
        introLabel.setFont(introFont);
        introLabel.setHorizontalAlignment(SwingConstants.LEFT);
        
        passphraseinput1.setPreferredSize( passphraseinput1.getPreferredSize() );
        passphraseinput1.getDocument().addDocumentListener(new DocumentListener() {
        	public void changedUpdate(DocumentEvent e) {
        		changed();
        	}
        	public void removeUpdate(DocumentEvent e) {
        		changed();
        	}
        	public void insertUpdate(DocumentEvent e) {
        		changed();
        	}
        	public void changed() {
        		if (passphraseinput1.getText().equals("")){
        			nextButton.setEnabled(false);
        		} else {
        			nextButton.setEnabled(true);
        		}
        	}
        });
        passphraseinput1.addAncestorListener( new RequestFocusListener() );
        
        helperLabel.setText("<html><br>Your passphrase may be 1 - 250 characters in length and is the security to your wallet. You must never forget it. If you lose your passphrase, you lose your wallet.<br><br></html>");
        helperLabel.setForeground(gold);
        helperLabel.setFont(introFont);
        
        backButton.setUI(new BasicButtonUI());
        backButton.setFont(buttonFont);
        backButton.setBackground(gold);
        backButton.setForeground(darkblue);
        backButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet1");
    		}
    	});
    	
    	nextButton.setUI(new BasicButtonUI());
    	nextButton.setFont(buttonFont);
    	nextButton.setBackground(gold);
    	nextButton.setForeground(darkblue);
    	nextButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			passphrase = passphraseinput1.getText();
    			((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet3");
    		}
    	});
    	if (passphraseinput1.getText().equals("")){
    		nextButton.setEnabled(false);
		} else {
			nextButton.setEnabled(true);
		}
        
         JPanel newPanel = new JPanel();
         newPanel.setOpaque(false);
         newPanel.setLayout(new GridBagLayout());
         newPanel.setBorder(new EmptyBorder(20,40,0,40));
         GridBagConstraints c = new GridBagConstraints();
         c.gridx = 0;
         c.gridy = 0;
         c.fill = GridBagConstraints.HORIZONTAL;
         c.gridwidth = 2;
         c.anchor = GridBagConstraints.FIRST_LINE_START;
         newPanel.add(introLabel, c);
         c.gridx = 0;
         c.gridy = 1;
         c.weightx = 1.0;
         c.fill = GridBagConstraints.HORIZONTAL;
         c.gridwidth = 2;
         c.anchor = GridBagConstraints.LINE_START;
         newPanel.add(passphraseinput1, c);
         c.gridx = 0;
         c.gridy = 2;
         c.weightx = 1.0;
         c.weighty = 1.0;
         c.gridwidth = 2;
         c.fill = GridBagConstraints.BOTH;
         c.anchor = GridBagConstraints.CENTER;
         newPanel.add(helperLabel, c);
         c.gridx = 0;
         c.gridy = 3;
         c.weightx = 0.5;
         c.gridwidth = 1;
         c.fill = GridBagConstraints.NONE;
         c.anchor = GridBagConstraints.LINE_END;
         c.insets = new Insets(0,0,0,10);
         newPanel.add(backButton, c);
         c.gridx = 1;
         c.gridy = 3;
         c.weightx = 0.5;
         c.gridwidth = 1;
         c.fill = GridBagConstraints.NONE;
         c.anchor = GridBagConstraints.LINE_START;
         c.insets = new Insets(0,10,0,0);
         newPanel.add(nextButton, c);
         return newPanel;
    }
    
    private static JPanel createWalletPanel3() {
    	final Font introFont = new Font("papyrus", Font.BOLD, 24);
        JButton nextButton = new JButton("Next");
        JLabel introLabel = new JLabel();
        JButton backButton = new JButton("Back");
        JTextField passphraseinput2 = new JTextField(255);
        JLabel helperLabel = new JLabel();
        
        introLabel.setText("<html>Re-Enter Your Passphrase:<br></html>");
        introLabel.setForeground(gold);
        introLabel.setFont(introFont);
        introLabel.setHorizontalAlignment(SwingConstants.LEFT);
        passphraseinput2.setMaximumSize( passphraseinput2.getPreferredSize() );
        passphraseinput2.getDocument().addDocumentListener(new DocumentListener() {
        	public void changedUpdate(DocumentEvent e) {
        		changed();
        	}
        	public void removeUpdate(DocumentEvent e) {
        		changed();
        	}
        	public void insertUpdate(DocumentEvent e) {
        		changed();
        	}
        	public void changed() {
        		if (passphraseinput2.getText().equals("")){
        			nextButton.setEnabled(false);
        		} else {
        			nextButton.setEnabled(true);
        		}
        	}
        });
        passphraseinput2.addAncestorListener( new RequestFocusListener() );
        
        helperLabel.setText("<html><br>You will be asked for your passphrase whenever accessing your wallet. Your passphrase is not stored anywhere in the wallet. It should be something that only you know and remember.<br><br></html>");
        helperLabel.setForeground(gold);
        helperLabel.setFont(introFont);
        helperLabel.setHorizontalAlignment(SwingConstants.LEFT);
        
        backButton.setUI(new BasicButtonUI());
        backButton.setFont(buttonFont);
        backButton.setBackground(gold);
        backButton.setForeground(darkblue);
        backButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet2");
    		}
    	});
    	
    	nextButton.setUI(new BasicButtonUI());
    	nextButton.setFont(buttonFont);
    	nextButton.setBackground(gold);
    	nextButton.setForeground(darkblue);
    	nextButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			if (passphrase.equals(passphraseinput2.getText())) {
    				helperLabel.setText("<html><br>You will be asked for your passphrase whenever accessing your wallet. Your passphrase is not stored anywhere in the wallet. It should be something that only you know and remember.<br><br></html>");
    				((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet4");
    			} else {
    				helperLabel.setText("<html><br>Passphrase does not match what you entered on the previous screen! Either enter the same passphrase above or go Back and re-enter your passphrase in Step 1.<br><br></html>");
    			}
    		}
    	});
    	if (passphraseinput2.getText().equals("")){
    		nextButton.setEnabled(false);
		} else {
			nextButton.setEnabled(true);
		}
        
    	JPanel newPanel = new JPanel();
        newPanel.setOpaque(false);
        newPanel.setLayout(new GridBagLayout());
        newPanel.setBorder(new EmptyBorder(20,40,0,40));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        newPanel.add(introLabel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.anchor = GridBagConstraints.LINE_START;
        newPanel.add(passphraseinput2, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        newPanel.add(helperLabel, c);
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_END;
        c.insets = new Insets(0,0,0,10);
        newPanel.add(backButton, c);
        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0,10,0,0);
        newPanel.add(nextButton, c);
        return newPanel;
    }
    
    private static JPanel createWalletPanel4() {
    	final Font introFont = new Font("papyrus", Font.BOLD, 24);
    	final Font instructionsFont = new Font("papyrus", Font.BOLD, 18);

        JLabel introLabel = new JLabel();
        JButton fileChooserButton = new JButton("SELECT FILE");
        JLabel helperLabel = new JLabel();
        JButton backButton = new JButton("Back");
        JButton nextButton = new JButton("Next");
        JPanel iconPanel = new JPanel();
        JLabel iconLabel = new JLabel();

        introLabel.setText("<html>Step 2.  Load an ORIGINAL image file:<br></html>");
        introLabel.setForeground(gold);
        introLabel.setFont(introFont);

        fileChooserButton.addActionListener(new ActionListener()
        { 
          public void actionPerformed(ActionEvent e)
          { 
        	  JFileChooser imageFileChooser = new JFileChooser();
              FileNameExtensionFilter imageFilter = new FileNameExtensionFilter("Images", "jpg", "gif", "jpeg", "png");
              imageFileChooser.setFileFilter(imageFilter);
              imageFileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
              imageFileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
              imageFileChooser.setPreferredSize( new Dimension(800, 400) );
              int result = imageFileChooser.showOpenDialog(null);
              if (result == JFileChooser.APPROVE_OPTION) {
                  entropyFile = imageFileChooser.getSelectedFile();
                  entropyIcon = createIcon(entropyFile,150,150);
                  iconLabel.setIcon(entropyIcon);
                  iconLabel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, darkblue));
              }
          } 
        });
        fileChooserButton.addAncestorListener( new RequestFocusListener() );

        helperLabel.setText("<html><br>The image should be a photo stored on your device that you have not shared with anyone else. If you do not have an orginal image, take a random photo of anything now and use that.<br><br></html>");
        helperLabel.setForeground(gold);
        helperLabel.setFont(instructionsFont);
        
        iconPanel.setOpaque(false);
        iconPanel.setLayout(new BorderLayout());
        iconPanel.setPreferredSize(new Dimension(150, 150));
        iconPanel.setMinimumSize(new Dimension(150, 150));
        JPanel iconLabelPanel = new JPanel();
        iconLabelPanel.setOpaque(false);
        iconLabelPanel.add(iconLabel);
        iconPanel.add(iconLabelPanel);
        
        JPanel rightPanel = new JPanel();
        rightPanel.setOpaque(false);
        rightPanel.setLayout(new BorderLayout(0,5));
        rightPanel.add(fileChooserButton, BorderLayout.NORTH);
        rightPanel.add(iconPanel, BorderLayout.SOUTH);

        backButton.setUI(new BasicButtonUI());
        backButton.setFont(buttonFont);
        backButton.setBackground(gold);
        backButton.setForeground(darkblue);
        backButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet3");
    		}
    	});
    	nextButton.setUI(new BasicButtonUI());
    	nextButton.setFont(buttonFont);
    	nextButton.setBackground(gold);
    	nextButton.setForeground(darkblue);
    	nextButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			if (entropyIcon != null) {
    				JPanel createWalletPanel5 = createWalletPanel5();
    		        preWalletPanel.add(createWalletPanel5,"createwallet5");
    		        ((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet5");
    			} else {
    				helperLabel.setText("<html><br>You must load a photo first.<br><br></html>");
    			}
    		}
    	});
        
        JPanel newPanel = new JPanel();
        newPanel.setOpaque(false);
        newPanel.setLayout(new GridBagLayout());
        newPanel.setBorder(new EmptyBorder(20,40,0,0));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.gridwidth = 2;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        newPanel.add(introLabel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.7;
        c.weighty = 1.0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        newPanel.add(helperLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.3;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.NONE;
        newPanel.add(rightPanel, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_END;
        c.insets = new Insets(0,0,0,10);
        newPanel.add(backButton, c);
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0,10,0,0);
        newPanel.add(nextButton, c);
        return newPanel;
    }
    
    private static JPanel createWalletPanel5() {
    	final Font introFont = new Font("papyrus", Font.BOLD, 18);
        JLabel introLabel = new JLabel();
        JLabel passphraseLabel = new JLabel();
        JLabel iconLabel = new JLabel();
        JPanel iconPanel = new JPanel();
        JButton backButton = new JButton("Back");
    	JButton nextButton = new JButton("Finish");

        introLabel.setText("<html>You are about to create your wallet. Please print this screen and store it somewhere safe. Your passphrase and image file will never be seen again.<br><br>Passphrase:</html>");
        introLabel.setForeground(gold);
        introLabel.setFont(introFont);
        introLabel.setHorizontalAlignment(SwingConstants.LEFT);
        
        iconPanel.setOpaque(false);
        iconPanel.setLayout(new BorderLayout());
        iconPanel.setPreferredSize(new Dimension(250, 250));
        iconPanel.setMinimumSize(new Dimension(250, 250));
        iconPanel.setMaximumSize(new Dimension(250, 250));
        JPanel iconLabelPanel = new JPanel();
        iconLabelPanel.setOpaque(false);
        iconLabelPanel.add(iconLabel);
        iconPanel.add(iconLabelPanel);
        
        if (entropyFile != null) {
        	entropyIcon = createIcon(entropyFile,250,250);
        	try {
				entropy = Files.readAllBytes(entropyFile.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        	iconLabel.setIcon(entropyIcon);
        	iconLabel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, gold));
        }
        
        passphraseLabel.setText(passphrase);
        passphraseLabel.setForeground(gold);
        passphraseLabel.setFont(introFont);
        
        backButton.setUI(new BasicButtonUI());
        backButton.setFont(buttonFont);
        backButton.setBackground(gold);
        backButton.setForeground(darkblue);
        backButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet4");
    		}
    	});
    	nextButton.setUI(new BasicButtonUI());
    	nextButton.setFont(buttonFont);
    	nextButton.setBackground(gold);
    	nextButton.setForeground(darkblue);
    	nextButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			if (entropy != null) {
    				byte[] salt = passphrase.getBytes(StandardCharsets.UTF_8);
    				ByteArrayOutputStream privatekeyentropy = new ByteArrayOutputStream();
    				try {
						privatekeyentropy.write(entropy);
						privatekeyentropy.write(salt);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
    				byte[] privatekey = Wallet.makePrivateKeyFromEntropy(privatekeyentropy.toByteArray());
    				createWallet(privatekey);
    				createWalletAddresses(privatekey);
    				saveWallet(walletFile);
    				((CardLayout)topPanel.getLayout()).show(topPanel, "main");
    			}
    		}
    	});
        
        JPanel newPanel = new JPanel();
        newPanel.setOpaque(true);
        newPanel.setBackground(darkdarkblue);
        newPanel.setLayout(new GridBagLayout());
        newPanel.setBorder(new EmptyBorder(20,40,0,0));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        newPanel.add(introLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        c.weighty = 1.0;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        newPanel.add(iconPanel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1.0;
        c.gridwidth = 2;
        c.gridheight = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.fill = GridBagConstraints.NONE;
        newPanel.add(passphraseLabel, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_END;
        c.insets = new Insets(0,0,0,10);
        newPanel.add(backButton, c);
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0,10,0,0);
        newPanel.add(nextButton, c);
        return newPanel;
    }
    
    public static JPanel headerPanel() {
    	
        final Color titleColor = lightblue;
        final Font titleFont = new Font("papyrus", Font.BOLD, 48);
        final Font subtitleFont = new Font("papyrus", Font.PLAIN, 18);
        
        JLabel titleLabel = new JLabel("BANSHEE");
        JLabel subtitleLabel = new JLabel("V e r g e    C u r r e n c y    W a l l e t           1 . 0");
        
        titleLabel.setForeground(titleColor);
        titleLabel.setFont(titleFont);
        titleLabel.setAlignmentX(SwingConstants.SOUTH_EAST);
        subtitleLabel.setForeground(titleColor);
        subtitleLabel.setFont(subtitleFont);
        subtitleLabel.setAlignmentX(SwingConstants.NORTH_EAST);
        
        JPanel titlePanel = new JPanel();
        titlePanel.setOpaque(false);
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
        titlePanel.add(titleLabel);
        //titlePanel.add(subtitleLabel);
        return titlePanel;

    }
    
    public static JPanel loginPanel() {
    	final Font introFont = new Font("papyrus", Font.BOLD, 24);
    	final Font timerFont = new Font("papyrus", Font.BOLD, 36);
    	
    	JPanel headerPanel = headerPanel();
    	JLabel introLabel = new JLabel();
        JTextField passphraseinput = new JTextField(255);
        
        JLabel timerLabel = new JLabel();
        timerLabel.setText("");
        timerLabel.setForeground(gold);
        timerLabel.setFont(timerFont);
        timerLabel.setHorizontalTextPosition(JLabel.CENTER);
        timerLabel.setIcon(new ImageIcon(Ui.class.getResource("timerbg.png")));
        
        introLabel.setText("<html>Wallet Passphrase:<br></html>");
        introLabel.setForeground(gold);
        introLabel.setFont(introFont);
        introLabel.setHorizontalAlignment(SwingConstants.LEFT);
        
        passphraseinput.setPreferredSize( passphraseinput.getPreferredSize() );
        passphraseinput.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	if (!passphraseinput.getText().equals("")) {
            		passphrase = passphraseinput.getText();
            		loadWallet();
            		if (wallet.security != null) {
            			if (wallet.security.equals("BANSHEE")) {
            				loggedIn = true;
            				timer.stop();
            				((CardLayout)topPanel.getLayout()).show(topPanel, "main");
            			} else {
            				System.out.println("Invalid password");
            			}
            		}
        		} else {
        			System.exit(0);
        		}
            }
        });
        passphraseinput.addAncestorListener( new RequestFocusListener() );
        
        timer = new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	if (startTime < 0) {
                    startTime = System.currentTimeMillis();
                }
                long now = System.currentTimeMillis();
                long clockTime = now - startTime;
                if (clockTime >= duration) {
                    clockTime = duration;
                    timer.stop();
                    if (!loggedIn) System.exit(0);
                }
                SimpleDateFormat df = new SimpleDateFormat("ss");
                timerLabel.setText(df.format(duration - clockTime));
            }
        });
        timer.setInitialDelay(0);
        timer.start();
        
    	JPanel loginpanel = new JPanel();
    	loginpanel.setOpaque(false);
    	loginpanel.setLayout(new GridBagLayout());
    	loginpanel.setBorder(new EmptyBorder(20,40,20,40));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.NORTHEAST;
        c.fill = GridBagConstraints.BOTH;
        loginpanel.add(headerPanel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.insets = new Insets(0,40,60,0);
        c.fill = GridBagConstraints.BOTH;
        loginpanel.add(timerLabel, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.insets = new Insets(0,0,0,0);
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        loginpanel.add(introLabel, c);
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.LINE_START;
        loginpanel.add(passphraseinput, c);
        
    	return loginpanel;
    }
    
    public static JPanel homePanel() {
    	JPanel headerPanel = headerPanel();
    	JPanel addressPanel = walletAddressPanel();
    	JPanel balancePanel = walletBalancePanel(walletBalance);
    	
    	JLabel mybalancelabel = new JLabel();
    	mybalancelabel.setText("<html><br><br><br><br>BALANCE:</html>");
    	mybalancelabel.setForeground(lightblue);
    	mybalancelabel.setFont(homepanelFont);
    	
    	//mywalletaddress.setText("xx");
    	if (walletExists()) {
    		
    	}
    	
    	JPanel homepanel = new JPanel();
    	homepanel.setOpaque(false);
    	homepanel.setLayout(new GridBagLayout());
    	homepanel.setBorder(new EmptyBorder(20,40,20,20));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(0,20,0,0);
        c.anchor = GridBagConstraints.SOUTHWEST;
        c.fill = GridBagConstraints.BOTH;
        homepanel.add(mybalancelabel, c);
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(0,0,0,0);
        c.anchor = GridBagConstraints.NORTHEAST;
        c.fill = GridBagConstraints.BOTH;
        homepanel.add(headerPanel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.1;
        c.weighty = 1.0;
        c.gridwidth = 2;
        c.insets = new Insets(0,20,0,0);
        c.anchor = GridBagConstraints.NORTHWEST;
        c.fill = GridBagConstraints.NONE;
        homepanel.add(balancePanel, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 1.0;
        c.gridwidth = 2;
        c.insets = new Insets(0,0,0,0);
        c.anchor = GridBagConstraints.SOUTHWEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        homepanel.add(addressPanel, c);
        return homepanel;
    }
    
    public static JPanel directoryPanel() {
    	JPanel directorypanel = new JPanel();
    	directorypanel.setOpaque(false);
    	directorypanel.setLayout(new GridBagLayout());
    	directorypanel.setBorder(new EmptyBorder(20,40,20,20));
        GridBagConstraints c = new GridBagConstraints();
        
        return directorypanel;
    }
    
    public static JPanel transactionsPanel() {
    	JPanel transactionspanel = new JPanel();
    	transactionspanel.setOpaque(false);
    	transactionspanel.setLayout(new GridBagLayout());
    	transactionspanel.setBorder(new EmptyBorder(20,40,20,20));
        GridBagConstraints c = new GridBagConstraints();
        
        return transactionspanel;
    }
    
    public static JPanel sendPanel() {
    	JPanel sendpanel = new JPanel();
    	sendpanel.setOpaque(false);
    	sendpanel.setLayout(new GridBagLayout());
    	sendpanel.setBorder(new EmptyBorder(20,40,20,20));
        GridBagConstraints c = new GridBagConstraints();
        
        return sendpanel;
    }
    
    public static JPanel backupPanel() {
    	JPanel backuppanel = new JPanel();
    	backuppanel.setOpaque(true);
    	backuppanel.setBackground(darkdarkblue);
    	backuppanel.setLayout(new GridBagLayout());
    	backuppanel.setBorder(new EmptyBorder(20,40,20,20));
        
        JLabel backupheaderlabel = new JLabel();
        backupheaderlabel.setOpaque(false);
        backupheaderlabel.setText("<html>Write down your private key and store it in a secure place. Your currency is always safe as long as you have your private key. You should backup your wallet file and store it in a secure place also.<br><br>Private Key:<br></html>");
        backupheaderlabel.setForeground(lightblue);
        backupheaderlabel.setFont(buttonFont);
        
        privatekeybackuptext = new JTextArea();
        privatekeybackuptext.setOpaque(false);
        privatekeybackuptext.setBorder(new EmptyBorder(1,1,1,1));
        privatekeybackuptext.setForeground(gold);
        //privatekeybackuptext.setFont(monoFont);
        privatekeybackuptext.setFont(new Font("monospaced", Font.PLAIN, 20));
        privatekeybackuptext.setColumns(16);
        privatekeybackuptext.setRows(4);
        privatekeybackuptext.setLineWrap(true);
        privatekeybackuptext.setWrapStyleWord(true);
        privatekeybackuptext.setText("");
        JPanel privatekeybackuptextpanel = new JPanel();
        privatekeybackuptextpanel.setOpaque(false);
        privatekeybackuptextpanel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, lightblue));
        privatekeybackuptextpanel.add(privatekeybackuptext);
        
        privatekeybackupqrcode = new JLabel();
        privatekeybackupqrcode.setOpaque(false);
        privatekeybackupqrcode.setForeground(gold);
        JPanel privatekeybackupqrcodepanel = new JPanel();
        privatekeybackupqrcodepanel.setOpaque(false);
        privatekeybackupqrcodepanel.add(privatekeybackupqrcode);

        JButton fileBackupButton = new JButton("BACKUP WALLET FILE TO ANOTHER LOCATION");
        fileBackupButton.addActionListener(new ActionListener()
        { 
          public void actionPerformed(ActionEvent e)
          { 
        	  JFileChooser backupFileChooser = new JFileChooser();
        	  backupFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        	  backupFileChooser.setDialogTitle("Backup Banshee Wallet");
        	  backupFileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        	  backupFileChooser.setSelectedFile(new File("BANSHEE.dat"));
        	  backupFileChooser.setPreferredSize( new Dimension(800, 400) );
        	  
              if (backupFileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                  File backupFile = backupFileChooser.getSelectedFile();
                  if (backupFile != null) {
                	  saveWallet(backupFile.getPath());
                  }
              }
          } 
        });
        fileBackupButton.addAncestorListener( new RequestFocusListener() );
        JPanel filebackupbuttonpanel = new JPanel();
        filebackupbuttonpanel.setOpaque(false);
        filebackupbuttonpanel.add(fileBackupButton);
        
        GridBagConstraints bpc = new GridBagConstraints();
        bpc.gridx = 0;
        bpc.gridy = 0;
        bpc.weightx = 1.0;
        bpc.gridwidth = 2;
        bpc.fill = GridBagConstraints.HORIZONTAL;
        bpc.anchor = GridBagConstraints.NORTHWEST;
        backuppanel.add(backupheaderlabel, bpc);
        bpc.gridx = 0;
        bpc.gridy = 1;
        bpc.weightx = 0.5;
        bpc.weighty = 1.0;
        bpc.gridwidth = 1;
        bpc.fill = GridBagConstraints.NONE;
        bpc.anchor = GridBagConstraints.NORTHWEST;
        backuppanel.add(privatekeybackuptextpanel, bpc);
        bpc.gridx = 1;
        bpc.gridy = 1;
        bpc.weightx = 0.5;
        bpc.weighty = 1.0;
        bpc.gridwidth = 1;
        bpc.fill = GridBagConstraints.HORIZONTAL;
        bpc.anchor = GridBagConstraints.NORTHWEST;
        backuppanel.add(privatekeybackupqrcodepanel, bpc);
        bpc.gridx = 0;
        bpc.gridy = 2;
        bpc.weightx = 1.0;
        bpc.gridwidth = 2;
        bpc.fill = GridBagConstraints.NONE;
        bpc.anchor = GridBagConstraints.NORTHWEST;
        backuppanel.add(filebackupbuttonpanel, bpc);

        return backuppanel;
    }
    
    public static JPanel restorePanel() {
    	String restoreinputstring = "";
    	
    	JPanel restorepanel = new JPanel();
    	restorepanel.setOpaque(false);
    	restorepanel.setLayout(new GridBagLayout());
    	restorepanel.setBorder(new EmptyBorder(20,40,20,20));
    	
        JPanel restoreInputPanel = new JPanel(new CardLayout());
        restoreInputPanel.setOpaque(false);
        
        JPanel restoreConfirmationCardsPanel = new JPanel(new CardLayout());
        restoreConfirmationCardsPanel.setOpaque(false);
        
    	JLabel headerlabel = new JLabel();
    	headerlabel.setText("<html>Restore your wallet using the primary key from your previous wallet, the entropy photo used to create your original wallet, or a Banshee database backup file. How would you like to restore your wallet?<br><br></html>");
    	headerlabel.setForeground(lightblue);
    	headerlabel.setFont(buttonFont);
    	
        JRadioButton restoreButtonKey = new JRadioButton("PRIMARY KEY");
        restoreButtonKey.setMnemonic(KeyEvent.VK_K);
        restoreButtonKey.setActionCommand("KEY");
        restoreButtonKey.setSelected(true);
        restoreButtonKey.setOpaque(false);
        restoreButtonKey.setForeground(gold);
        restoreButtonKey.setFont(homepanelFont);
        restoreButtonKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // run this if this button is pressed
            	((CardLayout)restoreInputPanel.getLayout()).show(restoreInputPanel, "key");
            	((CardLayout)restoreConfirmationCardsPanel.getLayout()).show(restoreConfirmationCardsPanel, "confirmrestoreblank");
            }
        });
        JRadioButton restoreButtonPhoto = new JRadioButton("ENTROPY PHOTO");
        restoreButtonPhoto.setMnemonic(KeyEvent.VK_P);
        restoreButtonPhoto.setActionCommand("PHOTO");
        restoreButtonPhoto.setSelected(true);
        restoreButtonPhoto.setOpaque(false);
        restoreButtonPhoto.setForeground(gold);
        restoreButtonPhoto.setFont(homepanelFont);
        restoreButtonPhoto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // run this if this button is pressed
            	((CardLayout)restoreInputPanel.getLayout()).show(restoreInputPanel, "photo");
            	((CardLayout)restoreConfirmationCardsPanel.getLayout()).show(restoreConfirmationCardsPanel, "confirmrestoreblank");
            }
        });
        JRadioButton restoreButtonFile = new JRadioButton("BANSHEE.DAT FILE");
        restoreButtonFile.setMnemonic(KeyEvent.VK_F);
        restoreButtonFile.setActionCommand("FILE");
        restoreButtonFile.setSelected(true);
        restoreButtonFile.setOpaque(false);
        restoreButtonFile.setForeground(gold);
        restoreButtonFile.setFont(homepanelFont);
        restoreButtonFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // run this if this button is pressed
            	((CardLayout)restoreInputPanel.getLayout()).show(restoreInputPanel, "file");
            	((CardLayout)restoreConfirmationCardsPanel.getLayout()).show(restoreConfirmationCardsPanel, "confirmrestoreblank");
            }
        });
        ButtonGroup restoreButtonGroup = new ButtonGroup();
        restoreButtonGroup.add(restoreButtonKey);
        restoreButtonGroup.add(restoreButtonPhoto);
        restoreButtonGroup.add(restoreButtonFile);
        JPanel buttongrouppanel = new JPanel();
        buttongrouppanel.setLayout(new BoxLayout(buttongrouppanel, BoxLayout.PAGE_AXIS));
        buttongrouppanel.setOpaque(false);
        buttongrouppanel.add(restoreButtonKey);
        buttongrouppanel.add(restoreButtonPhoto);
        buttongrouppanel.add(restoreButtonFile);
        
        // RADIO BUTTON PANELS END - INPUT PANELS BEGIN
        
        JPanel keyInputPanel = new JPanel();
        keyInputPanel.setOpaque(false);
        keyInputPanel.setLayout(new GridBagLayout());
        keyInputPanel.setBorder(new EmptyBorder(0,0,0,20));
        JLabel keyInputLabel = new JLabel();
        keyInputLabel.setText("Private Key:   ");
        keyInputLabel.setForeground(lightblue);
        keyInputLabel.setFont(buttonFont);
        JTextField keyInputTextField = new JTextField(255);
        keyInputTextField.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	privatekeyrestore = keyInputTextField.getText();
            	//((CardLayout)restoreConfirmationCardsPanel.getLayout()).show(restoreConfirmationCardsPanel, "confirmrestorekey");
            }
        });
        keyInputTextField.addAncestorListener( new RequestFocusListener() );
        JLabel passphraseKeyLabel = new JLabel();
        passphraseKeyLabel.setText("Passphrase:   ");
        passphraseKeyLabel.setForeground(lightblue);
        passphraseKeyLabel.setFont(buttonFont);
        passphraseKeyLabel.setHorizontalAlignment(SwingConstants.LEFT);
        JTextField passphraseKeyInput = new JTextField(255);
        passphraseKeyInput.setPreferredSize( passphraseKeyInput.getPreferredSize() );
        passphraseKeyInput.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	if (!passphraseKeyInput.getText().equals("")) {
            		restorepassphrase = passphraseKeyInput.getText();
            		System.out.println(restorepassphrase);
        		} else {

        		}
            }
        });
        JButton restoreKeyButton = new JButton("RESTORE");
        restoreKeyButton.setUI(new BasicButtonUI());
        restoreKeyButton.setFont(buttonFont);
        restoreKeyButton.setBackground(gold);
        restoreKeyButton.setForeground(darkblue);
        restoreKeyButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			if (!privatekeyrestore.equals("") && !restorepassphrase.equals("")) {
    				passphrase = restorepassphrase;
    				byte[] privatekey = privatekeyrestore.getBytes(StandardCharsets.UTF_8);
    				createWallet(privatekey);
    				createWalletAddresses(privatekey);
    				//wallet.setPrivateKey(privatekey);
    				saveWallet(walletFile);
    				((CardLayout)topPanel.getLayout()).show(topPanel, "main");
    			}
    		}
    	});
        GridBagConstraints kip = new GridBagConstraints();
        kip.gridx = 0;
        kip.gridy = 0;
        kip.weightx = 0.0;
        kip.anchor = GridBagConstraints.NORTHWEST;
        keyInputPanel.add(keyInputLabel, kip);
        kip.gridx = 1;
        kip.gridy = 0;
        kip.weightx = 1.0;
        kip.fill = GridBagConstraints.HORIZONTAL;
        keyInputPanel.add(keyInputTextField, kip);
        kip.gridx = 0;
        kip.gridy = 1;
        kip.weightx = 0.0;
        kip.anchor = GridBagConstraints.NORTHWEST;
        keyInputPanel.add(passphraseKeyLabel, kip);
        kip.gridx = 1;
        kip.gridy = 1;
        kip.weightx = 1.0;
        kip.fill = GridBagConstraints.HORIZONTAL;
        keyInputPanel.add(passphraseKeyInput, kip);
        kip.gridx = 0;
        kip.gridy = 2;
        kip.weightx = 0.0;
        kip.gridwidth = 2;
        kip.fill = GridBagConstraints.NONE;
        kip.anchor = GridBagConstraints.CENTER;
        keyInputPanel.add(restoreKeyButton, kip);
        
        JPanel photoInputPanel = new JPanel(new GridBagLayout());
        photoInputPanel.setOpaque(false);
        JLabel photoInputLabel = new JLabel();
        photoInputLabel.setText("Original Photo: ");
        photoInputLabel.setForeground(lightblue);
        photoInputLabel.setFont(buttonFont);
        JPanel photoInputRestorePanel = new JPanel(new CardLayout());
        photoInputRestorePanel.setOpaque(false);
        JPanel photoInputFilenamePanel = new JPanel(new BorderLayout());
        photoInputFilenamePanel.setOpaque(false);
        JLabel photoInputFilenameLabel = new JLabel();
        photoInputFilenameLabel.setForeground(gold);
        photoInputFilenameLabel.setFont(homepanelFont);
        photoInputFilenamePanel.add(photoInputFilenameLabel);
        photoInputRestorePanel.add(photoInputFilenamePanel, "photorestorefilename");
        
        JButton photoInputButton = new JButton("SELECT PHOTO FILE USED TO CREATE YOUR WALLET");
        photoInputButton.addActionListener(new ActionListener()
        { 
          public void actionPerformed(ActionEvent e)
          { 
        	  JFileChooser imageFileChooser = new JFileChooser();
              FileNameExtensionFilter imageFilter = new FileNameExtensionFilter("Images", "jpg", "gif", "jpeg", "png");
              imageFileChooser.setFileFilter(imageFilter);
              imageFileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
              imageFileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
              imageFileChooser.setPreferredSize( new Dimension(800, 400) );
              int result = imageFileChooser.showOpenDialog(null);
              if (result == JFileChooser.APPROVE_OPTION) {
                  entropyFile = imageFileChooser.getSelectedFile();
                  if (entropyFile != null) {
                	  try {
                		  entropy = Files.readAllBytes(entropyFile.toPath());
                		  backupentropyfile = entropyFile.getCanonicalPath();
                		  System.out.println(backupentropyfile);
                		  photoInputFilenameLabel.setText(backupentropyfile);
                		  ((CardLayout)photoInputRestorePanel.getLayout()).show(photoInputRestorePanel, "photorestorefilename");
                	  } catch (IOException e1) {
                		  e1.printStackTrace();
                	  }
                  }
              }
          } 
        });
        photoInputButton.addAncestorListener( new RequestFocusListener() );
        JPanel photoInputButtonPanel = new JPanel(new BorderLayout());
        photoInputButtonPanel.setOpaque(false);
        photoInputButtonPanel.add(photoInputButton);
        photoInputRestorePanel.add(photoInputButtonPanel, "photorestorebutton");
        ((CardLayout)photoInputRestorePanel.getLayout()).show(photoInputRestorePanel, "photorestorebutton");
        
        JLabel passphrasePhotoLabel = new JLabel();
        passphrasePhotoLabel.setText("Passphrase:   ");
        passphrasePhotoLabel.setForeground(lightblue);
        passphrasePhotoLabel.setFont(buttonFont);
        passphrasePhotoLabel.setHorizontalAlignment(SwingConstants.LEFT);
        JTextField passphrasePhotoInput = new JTextField(255);
        passphrasePhotoInput.setPreferredSize( passphrasePhotoInput.getPreferredSize() );
        passphrasePhotoInput.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	if (!passphrasePhotoInput.getText().equals("")) {
            		restorepassphrase = passphrasePhotoInput.getText();
            		System.out.println(restorepassphrase);
        		} else {

        		}
            }
        });
        JButton restorePhotoButton = new JButton("RESTORE");
        restorePhotoButton.setUI(new BasicButtonUI());
        restorePhotoButton.setFont(buttonFont);
        restorePhotoButton.setBackground(gold);
        restorePhotoButton.setForeground(darkblue);
        restorePhotoButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			if (entropy != null && !restorepassphrase.equals("")) {
					passphrase = restorepassphrase;
					byte[] salt = passphrase.getBytes(StandardCharsets.UTF_8);
    				ByteArrayOutputStream privatekeyentropy = new ByteArrayOutputStream();
    				try {
						privatekeyentropy.write(entropy);
						privatekeyentropy.write(salt);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
    				byte[] privatekey = Wallet.makePrivateKeyFromEntropy(privatekeyentropy.toByteArray());
					createWallet(privatekey);
	    			createWalletAddresses(privatekey);
	    			saveWallet(walletFile);
					System.out.println(backupentropyfile + " " + passphrase);
					((CardLayout)topPanel.getLayout()).show(topPanel, "main");
    			}
    		}
    	});
        GridBagConstraints pip = new GridBagConstraints();
        pip.gridx = 0;
        pip.gridy = 0;
        pip.weightx = 0.0;
        pip.anchor = GridBagConstraints.NORTHWEST;
        photoInputPanel.add(photoInputLabel, pip);
        pip.gridx = 1;
        pip.gridy = 0;
        pip.weightx = 1.0;
        pip.fill = GridBagConstraints.HORIZONTAL;
        photoInputPanel.add(photoInputRestorePanel, pip);
        pip.gridx = 0;
        pip.gridy = 1;
        pip.weightx = 0.0;
        pip.anchor = GridBagConstraints.NORTHWEST;
        photoInputPanel.add(passphrasePhotoLabel, pip);
        pip.gridx = 1;
        pip.gridy = 1;
        pip.weightx = 1.0;
        pip.fill = GridBagConstraints.HORIZONTAL;
        photoInputPanel.add(passphrasePhotoInput, pip);
        pip.gridx = 0;
        pip.gridy = 2;
        pip.weightx = 0.0;
        pip.gridwidth = 2;
        pip.fill = GridBagConstraints.NONE;
        pip.anchor = GridBagConstraints.CENTER;
        photoInputPanel.add(restorePhotoButton, pip);

        
        JPanel fileInputPanel = new JPanel(new GridBagLayout());
        fileInputPanel.setOpaque(false);
        JLabel fileInputLabel = new JLabel();
        fileInputLabel.setText("Restore File: ");
        fileInputLabel.setForeground(lightblue);
        fileInputLabel.setFont(buttonFont);
        JPanel fileInputRestorePanel = new JPanel(new CardLayout());
        fileInputRestorePanel.setOpaque(false);
        JPanel fileInputFilenamePanel = new JPanel(new BorderLayout());
        fileInputFilenamePanel.setOpaque(false);
        JLabel fileInputFilenameLabel = new JLabel();
        fileInputFilenameLabel.setForeground(gold);
        fileInputFilenameLabel.setFont(homepanelFont);
        fileInputFilenamePanel.add(fileInputFilenameLabel);
        fileInputRestorePanel.add(fileInputFilenamePanel, "filerestorefilename");
        JButton fileInputButton = new JButton("CLICK TO SELECT BANSHEE.DAT FILE");
        fileInputButton.addActionListener(new ActionListener()
        { 
          public void actionPerformed(ActionEvent e)
          { 
        	  JFileChooser databaseFileChooser = new JFileChooser();
        	  databaseFileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        	  databaseFileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        	  databaseFileChooser.setPreferredSize( new Dimension(800, 400) );
              if (databaseFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                  File databaseFile = databaseFileChooser.getSelectedFile();
                  if (databaseFile != null) {
                	  try {
                		  backupbansheedatfile = databaseFile.getCanonicalPath();
                		  System.out.println(backupbansheedatfile);
                		  fileInputFilenameLabel.setText(backupbansheedatfile);
                		  ((CardLayout)fileInputRestorePanel.getLayout()).show(fileInputRestorePanel, "filerestorefilename");
                	  } catch (IOException e1) {
                		  e1.printStackTrace();
                	  }
                  }
              }
          } 
        });
        fileInputButton.addAncestorListener( new RequestFocusListener() );
        JPanel fileInputButtonPanel = new JPanel(new BorderLayout());
        fileInputButtonPanel.setOpaque(false);
        fileInputButtonPanel.add(fileInputButton);
        fileInputRestorePanel.add(fileInputButtonPanel, "filerestorebutton");
        ((CardLayout)fileInputRestorePanel.getLayout()).show(fileInputRestorePanel, "filerestorebutton");
        
        JLabel passphraseFileLabel = new JLabel();
        passphraseFileLabel.setText("Passphrase:   ");
        passphraseFileLabel.setForeground(lightblue);
        passphraseFileLabel.setFont(buttonFont);
        passphraseFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
        JTextField passphraseFileInput = new JTextField(255);
        passphraseFileInput.setPreferredSize( passphraseFileInput.getPreferredSize() );
        passphraseFileInput.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	if (!passphraseFileInput.getText().equals("")) {
            		restorepassphrase = passphraseFileInput.getText();
            		System.out.println(restorepassphrase);
        		} else {

        		}
            }
        });
        JButton restoreFileButton = new JButton("RESTORE");
        restoreFileButton.setUI(new BasicButtonUI());
        restoreFileButton.setFont(buttonFont);
        restoreFileButton.setBackground(gold);
        restoreFileButton.setForeground(darkblue);
        restoreFileButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) { 
    			if (!backupbansheedatfile.equals("") && !restorepassphrase.equals("")) {
    				byte[] salt = Util.hashPassphrase(restorepassphrase);
    				SecretKey key64 = new SecretKeySpec( salt, "AES" );
    				Cipher cipher;
    				try {
    					cipher = Cipher.getInstance( "AES" );
    					cipher.init( Cipher.DECRYPT_MODE, key64 );
    					CipherInputStream cipherInputStream = new CipherInputStream( new BufferedInputStream( new FileInputStream( backupbansheedatfile ) ), cipher );
    					ObjectInputStream inputStream = new ObjectInputStream( cipherInputStream );
    					SealedObject sealedObject = (SealedObject) inputStream.readObject();
    					BansheeWallet testwallet = (BansheeWallet) sealedObject.getObject( cipher );
    					inputStream.close();
    					if (testwallet.security.equals("BANSHEE")) {
    						passphrase = restorepassphrase;
    						wallet = testwallet;
    						saveWallet(walletFile);
    						((CardLayout)topPanel.getLayout()).show(topPanel, "main");
    					}
    				} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | ClassNotFoundException | BadPaddingException | IOException e1) {
    					e1.printStackTrace();
    				}
    			}
    		}
    	});
        GridBagConstraints fip = new GridBagConstraints();
        fip.gridx = 0;
        fip.gridy = 0;
        fip.weightx = 0.0;
        fip.anchor = GridBagConstraints.NORTHWEST;
        fileInputPanel.add(fileInputLabel, fip);
        fip.gridx = 1;
        fip.gridy = 0;
        fip.weightx = 1.0;
        fip.fill = GridBagConstraints.HORIZONTAL;
        fileInputPanel.add(fileInputRestorePanel, fip);
        fip.gridx = 0;
        fip.gridy = 1;
        fip.weightx = 0.0;
        fip.anchor = GridBagConstraints.NORTHWEST;
        fileInputPanel.add(passphraseFileLabel, fip);
        fip.gridx = 1;
        fip.gridy = 1;
        fip.weightx = 1.0;
        fip.fill = GridBagConstraints.HORIZONTAL;
        fileInputPanel.add(passphraseFileInput, fip);
        fip.gridx = 0;
        fip.gridy = 2;
        fip.weightx = 0.0;
        fip.gridwidth = 2;
        fip.fill = GridBagConstraints.NONE;
        fip.anchor = GridBagConstraints.CENTER;
        fileInputPanel.add(restoreFileButton, fip);
        
        
        
        
        
        //fileInputPanel.add(fileInputLabel);
        //fileInputPanel.add(fileInputButton);

        // INPUT PANEL DEFINITIONS END
        
        JPanel confirmRestoreBlankPanel = new JPanel();
        confirmRestoreBlankPanel.setOpaque(false);
        
        JPanel confirmRestoreKeyPanel = new JPanel();
        confirmRestoreKeyPanel.setOpaque(false);
        confirmRestoreKeyPanel.setLayout(new GridBagLayout());
        confirmRestoreKeyPanel.setBorder(new EmptyBorder(0,0,0,20));
        JLabel confirmRestoreKeyLabel = new JLabel();
        confirmRestoreKeyLabel.setText("Private Key:");
        confirmRestoreKeyLabel.setForeground(lightblue);
        confirmRestoreKeyLabel.setFont(buttonFont);
        JLabel confirmRestoreInputKeyLabel = new JLabel();
        confirmRestoreInputKeyLabel.setText(restoreinputstring);
        confirmRestoreInputKeyLabel.setForeground(gold);
        confirmRestoreInputKeyLabel.setFont(buttonFont);
        JButton confirmRestoreKeyButton = new JButton("RESTORE");
        confirmRestoreKeyButton.addActionListener(new ActionListener()
        { 
          public void actionPerformed(ActionEvent e)
          {
        	  
          } 
        });
        GridBagConstraints e = new GridBagConstraints();
        e.gridx = 0;
        e.gridy = 0;
        e.gridwidth = 2;
        e.anchor = GridBagConstraints.NORTHWEST;
        e.fill = GridBagConstraints.BOTH;
        confirmRestoreKeyPanel.add(confirmRestoreKeyLabel, e);
        e.gridx = 0;
        e.gridy = 1;
        e.gridwidth = 1;
        e.fill = GridBagConstraints.BOTH;
        confirmRestoreKeyPanel.add(confirmRestoreInputKeyLabel, e);
        e.gridx = 1;
        e.gridy = 1;
        e.gridwidth = 1;
        e.fill = GridBagConstraints.HORIZONTAL;
        confirmRestoreKeyPanel.add(confirmRestoreKeyButton, e);
        
        

        
        
        
        
        restoreConfirmationCardsPanel.add(confirmRestoreBlankPanel,"confirmrestoreblank");
        restoreConfirmationCardsPanel.add(confirmRestoreKeyPanel,"confirmrestorekey");
        
        ((CardLayout)restoreConfirmationCardsPanel.getLayout()).show(restoreConfirmationCardsPanel, "confirmrestoreblank");
        
        restoreInputPanel.add(keyInputPanel,"key");
        restoreInputPanel.add(photoInputPanel,"photo");
        restoreInputPanel.add(fileInputPanel,"file");
        ((CardLayout)restoreInputPanel.getLayout()).show(restoreInputPanel, "key");

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.1;
        c.weighty = 0.1;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.fill = GridBagConstraints.BOTH;
        restorepanel.add(headerlabel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        restorepanel.add(buttongrouppanel, c);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.1;
        c.weighty = 0.1;
        c.fill = GridBagConstraints.HORIZONTAL;
        restorepanel.add(restoreInputPanel, c);/*
        c.gridx = 0;
        c.gridy = 3;
        c.fill = GridBagConstraints.BOTH;
        restorepanel.add(restoreConfirmationCardsPanel, c);
        */
    	return restorepanel;
    }
    
    public static JPanel postWalletMenuPanel() {

        final Font menuFont = new Font("papyrus", Font.BOLD, 10);
        final int menuItemWidth = 140;
        final int menuItemHeight = 30;
        final int menuItemSpace = 30;
        final Color menuColor = lightblue;
        JLabel homeLabel = new JLabel();
        JLabel directoryLabel = new JLabel();
        JLabel transactionsLabel = new JLabel();
        JLabel sendLabel = new JLabel();
        JLabel backupLabel = new JLabel();
        JLabel restoreLabel = new JLabel();
        JLabel closeLabel = new JLabel();

        homeLabel.setText("HOME");
        directoryLabel.setText("COINTACTS");
        transactionsLabel.setText("TRANSACTIONS");
        sendLabel.setText("SEND");
        backupLabel.setText("BACKUP");
        restoreLabel.setText("RESTORE");
        closeLabel.setText("CLOSE");
        
        JPanel postWalletMenuPanel = new JPanel();
        postWalletMenuPanel.setOpaque(false);
        postWalletMenuPanel.setLayout(new BoxLayout(postWalletMenuPanel, BoxLayout.PAGE_AXIS));
        postWalletMenuPanel.setBorder(new EmptyBorder(0,20,0,0));

        homeLabel.setForeground(menuColor);
        homeLabel.setFont(menuFont);
        homeLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        homeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        homeLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)walletPanel.getLayout()).show(walletPanel, "home");
            }

        });
        postWalletMenuPanel.add(homeLabel);
        postWalletMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );

        directoryLabel.setForeground(menuColor);
        directoryLabel.setFont(menuFont);
        directoryLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        directoryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        directoryLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)walletPanel.getLayout()).show(walletPanel, "directory");
            }

        });
        postWalletMenuPanel.add(directoryLabel);
        postWalletMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        
        transactionsLabel.setForeground(menuColor);
        transactionsLabel.setFont(menuFont);
        transactionsLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        transactionsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        transactionsLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)walletPanel.getLayout()).show(walletPanel, "transactions");
            }

        });
        postWalletMenuPanel.add(transactionsLabel);
        postWalletMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        
        sendLabel.setForeground(menuColor);
        sendLabel.setFont(menuFont);
        sendLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        sendLabel.setHorizontalAlignment(SwingConstants.CENTER);
        sendLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)walletPanel.getLayout()).show(walletPanel, "send");
            }

        });
        postWalletMenuPanel.add(sendLabel);
        postWalletMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        
        backupLabel.setForeground(menuColor);
        backupLabel.setFont(menuFont);
        backupLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        backupLabel.setHorizontalAlignment(SwingConstants.CENTER);
        backupLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)walletPanel.getLayout()).show(walletPanel, "backup");
            }

        });
        postWalletMenuPanel.add(backupLabel);
        postWalletMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        
        restoreLabel.setForeground(menuColor);
        restoreLabel.setFont(menuFont);
        restoreLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        restoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        restoreLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)walletPanel.getLayout()).show(walletPanel, "restore");
            }

        });
        postWalletMenuPanel.add(restoreLabel);
        postWalletMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        
        closeLabel.setForeground(menuColor);
        closeLabel.setFont(menuFont);
        closeLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        closeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        closeLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	System.exit(0);
            }

        });
        postWalletMenuPanel.add(closeLabel);
        return postWalletMenuPanel;
        
    }

    public static JPanel preWalletMenuPanel() {

        final Font menuFont = new Font("papyrus", Font.BOLD, 10);
        final int menuItemWidth = 140;
        final int menuItemHeight = 30;
        final int menuItemSpace = 30;
        final Color menuColor = lightblue;
        final Color preMenuColor = greyedout;
        JLabel homeLabel = new JLabel();
        JLabel directoryLabel = new JLabel();
        JLabel transactionsLabel = new JLabel();
        JLabel sendLabel = new JLabel();
        JLabel backupLabel = new JLabel();
        JLabel restoreLabel = new JLabel();
        JLabel closeLabel = new JLabel();

        homeLabel.setText("HOME");
        directoryLabel.setText("COINTACTS");
        transactionsLabel.setText("TRANSACTIONS");
        sendLabel.setText("SEND");
        backupLabel.setText("BACKUP");
        restoreLabel.setText("RESTORE");
        closeLabel.setText("CLOSE");
        
        JPanel preMenuPanel = new JPanel();
        preMenuPanel.setOpaque(false);
        preMenuPanel.setLayout(new BoxLayout(preMenuPanel, BoxLayout.PAGE_AXIS));
        preMenuPanel.setBorder(new EmptyBorder(0,20,0,0));

        homeLabel.setForeground(menuColor);
        homeLabel.setFont(menuFont);
        homeLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        homeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        homeLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "createwallet1");
            }

        });
        preMenuPanel.add(homeLabel);
        preMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );

        directoryLabel.setForeground(preMenuColor);
        directoryLabel.setFont(menuFont);
        directoryLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        directoryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        preMenuPanel.add(directoryLabel);
        preMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        MouseListener[] directoryLabelMouseListeners = directoryLabel.getMouseListeners();
        for (int i = 0; i < directoryLabelMouseListeners.length; i++) {
        	directoryLabel.removeMouseListener(directoryLabelMouseListeners[i]);
        }

        transactionsLabel.setForeground(preMenuColor);
        transactionsLabel.setFont(menuFont);
        transactionsLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        transactionsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        preMenuPanel.add(transactionsLabel);
        preMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        MouseListener[] transactionsLabelMouseListeners = transactionsLabel.getMouseListeners();
        for (int i = 0; i < transactionsLabelMouseListeners.length; i++) {
        	transactionsLabel.removeMouseListener(transactionsLabelMouseListeners[i]);
        }
        
        sendLabel.setForeground(preMenuColor);
        sendLabel.setFont(menuFont);
        sendLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        sendLabel.setHorizontalAlignment(SwingConstants.CENTER);
        preMenuPanel.add(sendLabel);
        preMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        MouseListener[] sendLabelMouseListeners = sendLabel.getMouseListeners();
        for (int i = 0; i < sendLabelMouseListeners.length; i++) {
        	sendLabel.removeMouseListener(sendLabelMouseListeners[i]);
        }
        
        backupLabel.setForeground(preMenuColor);
        backupLabel.setFont(menuFont);
        backupLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        backupLabel.setHorizontalAlignment(SwingConstants.CENTER);
        preMenuPanel.add(backupLabel);
        preMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        MouseListener[] backupLabelMouseListeners = backupLabel.getMouseListeners();
        for (int i = 0; i < backupLabelMouseListeners.length; i++) {
        	backupLabel.removeMouseListener(backupLabelMouseListeners[i]);
        }

        restoreLabel.setForeground(menuColor);
        restoreLabel.setFont(menuFont);
        restoreLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        restoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        restoreLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	((CardLayout)preWalletPanel.getLayout()).show(preWalletPanel, "restore");
            }

        });
        preMenuPanel.add(restoreLabel);
        preMenuPanel.add( Box.createVerticalStrut(menuItemSpace) );
        
        closeLabel.setForeground(menuColor);
        closeLabel.setFont(menuFont);
        closeLabel.setMaximumSize(new Dimension(menuItemWidth,menuItemHeight));
        closeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        closeLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	System.exit(0);
            }

        });
        preMenuPanel.add(closeLabel);
        return preMenuPanel;
        
    }
    
    private static void createWallet(byte[] privatekey) {
		List<Cointact> cointacts = new ArrayList<Cointact>();
		List<Log> logs = new ArrayList<Log>();
		String security = "BANSHEE";
		wallet = new BansheeWallet(privatekey, cointacts, logs, security);
    }
    
    public static void createWalletAddresses(byte[] privatekey) {
		walletimportformatkey = Wallet.makeWalletImportFormatKey(privatekey);
		compressedwalletimportformatkey = Wallet.makeCompressedWalletImportFormatKey(privatekey);
		publickey = Wallet.makePublicKeyFromPrivateKey(privatekey);
		compressedpublickey = Wallet.makeCompressedPublicKeyFromPrivateKey(privatekey);
		address = Wallet.makeAddress(publickey);
	}

	public static void saveWallet(String walletfile) {
		if (!passphrase.equals("")) {
			byte[] salt = Util.hashPassphrase(passphrase);
			SecretKey key64 = new SecretKeySpec( salt, "AES" );
			Cipher cipher;
			try {
				cipher = Cipher.getInstance( "AES" );
				cipher.init( Cipher.ENCRYPT_MODE, key64 );
				SealedObject sealedObject = new SealedObject( wallet, cipher);
				CipherOutputStream cipherOutputStream = new CipherOutputStream( new BufferedOutputStream( new FileOutputStream( walletfile ) ), cipher );
				ObjectOutputStream outputStream = new ObjectOutputStream( cipherOutputStream );
				outputStream.writeObject( sealedObject );
				outputStream.close();
			} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public static void loadWallet() {
		if (!passphrase.equals("")) {
			byte[] salt = Util.hashPassphrase(passphrase);
			SecretKey key64 = new SecretKeySpec( salt, "AES" );
			Cipher cipher;
			try {
				cipher = Cipher.getInstance( "AES" );
				cipher.init( Cipher.DECRYPT_MODE, key64 );
				CipherInputStream cipherInputStream = new CipherInputStream( new BufferedInputStream( new FileInputStream( walletFile ) ), cipher );
				ObjectInputStream inputStream = new ObjectInputStream( cipherInputStream );
				SealedObject sealedObject = (SealedObject) inputStream.readObject();
				BansheeWallet testwallet = (BansheeWallet) sealedObject.getObject( cipher );
				inputStream.close();
				if (testwallet.security.equals("BANSHEE")) {
					wallet = testwallet;
					updateUiPrivateKey();
				}
			} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | ClassNotFoundException | BadPaddingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Invalid password on loadWallet()");
			}
			
		}
	}
	
	private static void updateUiPrivateKey() {
		String privatekey = Util.bytesToHex(wallet.privatekey);
		privatekeybackuptext.setText(privatekey);
		//QrCode qr = QrCode.encodeText(privatekey, QrCode.Ecc.LOW);
		QrCode qr = QrCode.encodeBinary(wallet.privatekey, QrCode.Ecc.LOW);
		BufferedImage privatekeyqrcode = qr.toImage(4, 2);
		privatekeybackupqrcode.setIcon(new ImageIcon(privatekeyqrcode));
		System.out.println("Private Key: " + privatekey);
		byte[] pubkey = Wallet.makePublicKeyFromPrivateKey(wallet.privatekey);
		String addr = Wallet.makeAddress(pubkey);
		mywalletaddress.setText("xxx");
	}
	
    private static ImageIcon createIcon(File source,int maxHeight, int maxWidth) {
    	int newHeight = 0, newWidth = 0;
        int priorHeight = 0, priorWidth = 0;
        BufferedImage image = null;
        ImageIcon sizeImage;
        try {
                image = ImageIO.read(source);
        } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Picture upload attempted & failed");
        }
        sizeImage = new ImageIcon(image);
        if(sizeImage != null)
        {
            priorHeight = sizeImage.getIconHeight(); 
            priorWidth = sizeImage.getIconWidth();
        }
        if((float)priorHeight/(float)priorWidth > (float)maxHeight/(float)maxWidth)
        {
            newHeight = maxHeight;
            newWidth = (int)(((float)priorWidth/(float)priorHeight)*(float)newHeight);
        }
        else 
        {
            newWidth = maxWidth;
            newHeight = (int)(((float)priorHeight/(float)priorWidth)*(float)newWidth);
        }
        BufferedImage resizedImg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(image, 0, 0, newWidth, newHeight, null);
        g2.dispose();
        return (new ImageIcon(resizedImg));
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    ex.printStackTrace();
                }

                registerFonts();
                doesWalletExist();

                JFrame frame = new Ui();
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

}