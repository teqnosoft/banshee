import java.time.ZonedDateTime;

public class Log {

	private ZonedDateTime when;
	private String message;
	
	public Log (ZonedDateTime when, String message) {
		this.when = when;
		this.message = message;
	}
	
	public ZonedDateTime getWhen() {
		return this.when;
	}
	
	public String getMessage() {
		return this.message;
	}
	
}
